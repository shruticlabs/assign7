package com.example.shruti.assignment7.activities;

import android.annotation.TargetApi;

import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shruti.assignment7.R;


public class MainActivity extends ActionBarActivity {

    CustomTouchListener touch;
    TextView viewText;
    Button circle[] = new Button[6];
    int count[] = new int[6];
    static int previousState;
    View lowerLayout;
    RelativeLayout parent;
    int i = 0, radius;
    int centreX[] = new int[6];
    int centreY[] = new int[6];
    int point[] = new int[6];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewText = (TextView) findViewById(R.id.view_text);
        parent = (RelativeLayout) findViewById(R.id.sub_layout);

        for (int i = 0; i < parent.getChildCount(); i++) {

            circle[i] = (Button) parent.getChildAt(i);
        }


        ViewTreeObserver vto = parent.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                //noinspection deprecation
                parent.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                radius = (int) (circle[0].getWidth() / 2);
                for (i = 0; i < 6; i++) {
                    centreX[i] = (int) circle[i].getLeft() + radius;
                    centreY[i] = (int) circle[i].getTop() + radius;
                }

            }
        });

        lowerLayout = (View) findViewById(R.id.lower_layout);
        touch = new CustomTouchListener(lowerLayout);
        lowerLayout.setOnTouchListener(touch);

    }

    public class CustomTouchListener implements View.OnTouchListener {
        public CustomTouchListener(View lowerLayout) {
            View v = lowerLayout;
        }


        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public boolean onTouch(View v, MotionEvent event) {

            int x, y;

            switch (event.getAction()) {

                case MotionEvent.ACTION_DOWN:
                    x = (int) event.getX();
                    y = (int) event.getY();
                    viewText.setText("X:" + x + ", Y:" + y);
                    for (int l = 0; l <= 5; l++) {
                        point[l] = (int) Math.sqrt(Math.pow(x - centreX[l], 2) + Math.pow(y - centreY[l], 2));
                    }
                    int j=0;
                    while (j < point.length) {
                        if (point[j] < (radius)) {
                            Toast.makeText(getApplicationContext(), "Inside Circle",
                                    Toast.LENGTH_SHORT).show();
                            count[j] = count[j] + 1;
                            if ((count[j] % 2) == 0) {
                                circle[j].setBackground(getResources().getDrawable(R.drawable.black_btn));
                            } else {
                                circle[j].setBackground(getResources().getDrawable(R.drawable.red_btn));
                            }
                        }
                        j++;
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    x = (int) event.getX();
                    y = (int) event.getY();
                    viewText.setText("X:" + x + ", Y:" + y);
                    break;

                case MotionEvent.ACTION_MOVE:
                    x = (int) event.getX();
                    y = (int) event.getY();
                    viewText.setText("X:" + x + ", Y:" + y);
                    for (i = 0; i <= 5; i++) {
                        point[i] = (int) Math.sqrt(Math.pow(x - centreX[i], 2) + Math.pow(y - centreY[i], 2));
                    }
                    int k=0;
                    while (k < point.length) {
                        if (point[k] < (radius)) {
                            Toast.makeText(getApplicationContext(), "Inside Circle",
                                    Toast.LENGTH_SHORT).show();
                            count[k] = count[k] + 1;
                            if (previousState == k) {
                                Toast.makeText(getApplicationContext(), "Inside Same Circle",
                                        Toast.LENGTH_SHORT).show();
                            } else if ((count[k] % 2) == 0) {
                                circle[k].setBackground(getResources().getDrawable(R.drawable.black_btn));
                            } else {
                                circle[k].setBackground(getResources().getDrawable(R.drawable.red_btn));
                            }
                            previousState = k;
                        }
                        k++;
                    }
                    break;
               }
            return true;
        }
    }
}
